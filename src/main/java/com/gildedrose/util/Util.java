package com.gildedrose.util;

/**
 * @author wilfr
 *
 */
public class Util {

private Util() {
		
	}
	
	public static final String BACKSTAGE_PASSES_ITEM_NAME = "Backstage passes to a TAFKAL80ETC concert";
	public static final String SULFURAS_ITEM_NAME = "Sulfuras, Hand of Ragnaros";
	public static final String AGED_BRIE_ITEM_NAME = "Aged Brie";
	public static final String CONJURED_ITEM_NAME = "Conjured Mana Cake";
	
	public static final int DEFAULT_DECREASE = 1;
	public static final int NO_DECREASE = 0;
	
	public static final int MAX_QUALITY_VALUE = 50;
	public static final int DEFAULT_QUALITY_INC = 1;
	public static final int DEFAULT_QUALITY_DEC = 1;
	
	public static final int TEN_DAYS = 10;
	public static final int FIVE_DAYS = 5;

	public static final int NO_EXTRA_QUALITY_INC = 0;
	public static final int DAYS_6_10_QUALITY_INC = 2;
	public static final int DAYS_1_5_QUALITY_INC = 3;
	
	public static final int CONJURED_QUALITY_DEC = DEFAULT_QUALITY_DEC * 2;
}
