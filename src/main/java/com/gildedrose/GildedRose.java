package com.gildedrose;

import java.util.List;

import com.gildedrose.itemcontrol.quality.QualityControlService;
import com.gildedrose.itemcontrol.sellin.SellInControl;



class GildedRose {
	private QualityControlService qualityControService;
	private SellInControl sellInControl;
	
	public GildedRose(QualityControlService qualityControService, SellInControl sellInControl) {
		this.qualityControService = qualityControService;
		this.sellInControl = sellInControl;
	}

	public void updateQuality(List<Item> items) {
		for (Item item : items) {
			udpateItemSellIn(item);
			qualityControService.updateQuality(item);
		}
	}
	
	private void udpateItemSellIn(Item item) {
		sellInControl.updateItemSellIn(item);
	}
}