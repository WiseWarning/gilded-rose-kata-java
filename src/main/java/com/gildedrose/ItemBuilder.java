package com.gildedrose;

public class ItemBuilder {
	
	private String name;
	private int sellIn;
	private int quality;
	
	public static ItemBuilder initBuilder() {
		return new ItemBuilder().quality(1).sellIn(1);
	}

	public ItemBuilder quality(int quality) {
		this.quality = quality;
		return this;
	}

	public ItemBuilder sellIn(int sellIn) {
		this.sellIn = sellIn;
		return this;
	}

	public ItemBuilder name(String name) {
		this.name = name;
		return this;
	}

	public Item build() {
		return new Item(name, sellIn, quality);
	}

}
