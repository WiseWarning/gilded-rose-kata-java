package com.gildedrose.itemcontrol.sellin;

import static com.gildedrose.util.Util.SULFURAS_ITEM_NAME;
import static com.gildedrose.util.Util.NO_DECREASE;
import static com.gildedrose.util.Util.DEFAULT_DECREASE;

import com.gildedrose.Item;

public class SellInControl {

	public void updateItemSellIn(Item item) {
		item.sellIn -= itemSellInDecrease(item);
	}

	private int itemSellInDecrease(Item item) {
		return SULFURAS_ITEM_NAME.equals(item.name) ? NO_DECREASE : DEFAULT_DECREASE;
	}

}