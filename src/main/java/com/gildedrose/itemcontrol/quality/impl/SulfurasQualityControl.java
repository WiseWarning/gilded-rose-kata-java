package com.gildedrose.itemcontrol.quality.impl;

import com.gildedrose.Item;
import com.gildedrose.itemcontrol.quality.itf.IQualityControl;

/**
 * @author wilfr
 *
 */
public class SulfurasQualityControl  implements IQualityControl  {

	/**
	 * Being a legendary item, never has to be sold or decreases in Quality.
	 * */
	@Override
	public void updateItemQuality(Item item) {
		// being a legendary item, never has to be sold or decreases in Quality.
		
	}

}
