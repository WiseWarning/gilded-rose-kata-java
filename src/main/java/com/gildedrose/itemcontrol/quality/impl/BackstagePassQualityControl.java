package com.gildedrose.itemcontrol.quality.impl;

import static java.lang.Math.min;
import static com.gildedrose.util.Util.*;

import com.gildedrose.Item;
import com.gildedrose.itemcontrol.quality.itf.IQualityControl;

/**
 * @author wilfr
 *
 */
public class BackstagePassQualityControl implements IQualityControl {

	@Override
	public void updateItemQuality(Item item) {
		item.quality= updatedQuality(item);
	}

	private int updatedQuality(Item item) {
		return item.sellIn > 0 ? min(item.quality + increasedValue(item), MAX_QUALITY_VALUE) : 0;
	}

	private int increasedValue(Item item) {
		boolean testDaysBetween6And10 = item.sellIn > FIVE_DAYS && item.sellIn <= TEN_DAYS;
		if (testDaysBetween6And10) {
			return DAYS_6_10_QUALITY_INC;
		} else if (concertIsInFiveOrLessDays(item)) {
			return DAYS_1_5_QUALITY_INC;
		} else if (item.sellIn > 10) {
			return DEFAULT_QUALITY_INC;
		}
		return NO_EXTRA_QUALITY_INC;
	}

	private boolean concertIsInFiveOrLessDays(Item item) {
		return item.sellIn <= FIVE_DAYS && item.sellIn > 0;
	}

}
