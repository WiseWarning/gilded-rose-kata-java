package com.gildedrose.itemcontrol.quality.impl;

import static java.lang.Math.min;
import static com.gildedrose.util.Util.*;

import com.gildedrose.Item;
import com.gildedrose.itemcontrol.quality.itf.IQualityControl;

/**
 * @author wilfr
 *
 */
public class AgedBrieQualityControl implements IQualityControl {

	@Override
	public void updateItemQuality(Item item) {
		item.quality= udpatedQuality(item);
	}

	private int udpatedQuality(Item item) {
		return min(item.quality + DEFAULT_QUALITY_INC, MAX_QUALITY_VALUE);
	}

}
