package com.gildedrose.itemcontrol.quality.impl;

import com.gildedrose.Item;
import com.gildedrose.itemcontrol.quality.itf.IQualityControl;

import static com.gildedrose.util.Util.*;
/**
 * @author wilfr
 *
 */
public class DefaultQualityControl implements IQualityControl {

	@Override
	public void updateItemQuality(Item item) {
		item.quality -= decreasedValue(item);
	}

	private int decreasedValue(Item item) {
		int defaultQualityDrop = defaultQualityDec(item);
		return item.quality - defaultQualityDrop >= 0 ? defaultQualityDrop : item.quality;
	}

	private int defaultQualityDec(Item item) {
		return item.sellIn < 0 ? DEFAULT_QUALITY_DEC * 2 : DEFAULT_QUALITY_DEC;
	}

}