package com.gildedrose.itemcontrol.quality.impl;

import com.gildedrose.Item;
import com.gildedrose.itemcontrol.quality.itf.IQualityControl;

import static com.gildedrose.util.Util.*;

/**
 * @author wilfr
 *
 */
public class ConjuredQualityControl implements IQualityControl {

	@Override
	public void updateItemQuality(Item item) {
		item.quality -= decresedValue(item);
	}

	private int decresedValue(Item item) {
		return item.quality - CONJURED_QUALITY_DEC > 0 ? CONJURED_QUALITY_DEC : item.quality;

	}
}
