package com.gildedrose.itemcontrol.quality;

import com.gildedrose.Item;
import com.gildedrose.itemcontrol.quality.impl.AgedBrieQualityControl;
import com.gildedrose.itemcontrol.quality.impl.ConjuredQualityControl;
import com.gildedrose.itemcontrol.quality.impl.BackstagePassQualityControl;
import com.gildedrose.itemcontrol.quality.impl.SulfurasQualityControl;
import com.gildedrose.itemcontrol.quality.impl.DefaultQualityControl;
import com.gildedrose.itemcontrol.quality.itf.IQualityControl;
import static com.gildedrose.util.Util.*;

public class QualityControlService {

	private enum ItemSpecificStrategies {
		AGED_BRIE(AGED_BRIE_ITEM_NAME, new AgedBrieQualityControl()),
		CONJURED(CONJURED_ITEM_NAME, new ConjuredQualityControl()),
		BACKSTAGE_PASS(BACKSTAGE_PASSES_ITEM_NAME, new BackstagePassQualityControl()),
		SULFURAS(SULFURAS_ITEM_NAME, new SulfurasQualityControl());

		private String itemName;
		private IQualityControl qualityControl;

		private ItemSpecificStrategies(String itemName, IQualityControl qualityControl) {
			this.itemName = itemName;
			this.qualityControl = qualityControl;
		}
	}

	public void updateQuality(Item item) {
		for (ItemSpecificStrategies itemQualityControl : ItemSpecificStrategies.values()) {
			if (itemQualityControl.itemName.equalsIgnoreCase(item.name)) {
				itemQualityControl.qualityControl.updateItemQuality(item);
				break;
			}
		}
		new DefaultQualityControl().updateItemQuality(item);
	}

}
