package com.gildedrose.itemcontrol.quality.itf;

import com.gildedrose.Item;

public interface IQualityControl {

	void updateItemQuality(Item item);
}
