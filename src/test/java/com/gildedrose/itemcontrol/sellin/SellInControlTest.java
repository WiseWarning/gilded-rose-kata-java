package com.gildedrose.itemcontrol.sellin;

import static org.junit.jupiter.api.Assertions.*;
import static com.gildedrose.ItemBuilder.initBuilder;
import static com.gildedrose.util.Util.SULFURAS_ITEM_NAME;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.gildedrose.Item;

class SellInControlTest {

	private SellInControl sellInControl;

	@BeforeEach
	public void init() {
		sellInControl = new SellInControl();
	}
	
	@Test public void
	shouldDecreaseSellInInOne() {
		Item item = initBuilder().name("+5 Dexterity Vest").sellIn(10).build();
		
		sellInControl.updateItemSellIn(item);
		
		assertEquals(9, item.sellIn);
	}

	@Test public void
	shouldNotDecreaseSellInForSulfuras() {
		Item item = initBuilder().name(SULFURAS_ITEM_NAME).sellIn(10).build();
		
		sellInControl.updateItemSellIn(item);
		
		assertEquals(10, item.sellIn);
	}

}
