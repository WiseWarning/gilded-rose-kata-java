package com.gildedrose.itemcontrol.quality.impl;

import static org.junit.jupiter.api.Assertions.*;
import static com.gildedrose.util.Util.AGED_BRIE_ITEM_NAME;
import static com.gildedrose.ItemBuilder.initBuilder;

import org.junit.jupiter.api.Test;

import com.gildedrose.Item;

class AgedBrieQualityControlTest {


	private AgedBrieQualityControl agedBrieQualityControl = new AgedBrieQualityControl();

	@Test
	public void shouldIncreaseAgedBrieWheverItGetsOlder() {
		Item agedBrie = initBuilder().name(AGED_BRIE_ITEM_NAME).quality(15).build();

		agedBrieQualityControl.updateItemQuality(agedBrie);
		assertEquals(16, agedBrie.quality);
	}

	@Test
	public void shouldNeverIncreaseTheQualityToMoreThanFifty() {
		Item agedBrie = initBuilder().name(AGED_BRIE_ITEM_NAME).quality(50).build();

		agedBrieQualityControl.updateItemQuality(agedBrie);

		assertEquals(50, agedBrie.quality);
	}
	
	@Test
	public void shouldIncreaseAgedBrieQuantity() {
		Item agedBrie = initBuilder().name(AGED_BRIE_ITEM_NAME).quality(0).sellIn(2).build();

		agedBrieQualityControl.updateItemQuality(agedBrie);

		assertEquals(1, agedBrie.quality);
	}

}
