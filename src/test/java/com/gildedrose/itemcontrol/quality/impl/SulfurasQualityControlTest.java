package com.gildedrose.itemcontrol.quality.impl;

import static org.junit.jupiter.api.Assertions.*;
import static com.gildedrose.ItemBuilder.initBuilder;
import static com.gildedrose.util.Util.SULFURAS_ITEM_NAME;

import org.junit.jupiter.api.Test;

import com.gildedrose.Item;

class SulfurasQualityControlTest {

	private SulfurasQualityControl sulfurasQualityControl = new SulfurasQualityControl();

	@Test
	public void shouldNeverChangeQuality() {
		Item sulfuras = initBuilder().name(SULFURAS_ITEM_NAME).quality(80).sellIn(10).build();

		sulfurasQualityControl.updateItemQuality(sulfuras);

		assertEquals(80, sulfuras.quality);
	}

	@Test
	public void shouldAlsoNeverChangeQuality() {
		Item sulfuras = initBuilder().name(SULFURAS_ITEM_NAME).quality(80).sellIn(0).build();

		sulfurasQualityControl.updateItemQuality(sulfuras);

		assertEquals(80, sulfuras.quality);
	}

	@Test
	public void shouldNeverChangeQualityToo() {
		Item sulfuras = initBuilder().name(SULFURAS_ITEM_NAME).quality(80).sellIn(-1).build();

		sulfurasQualityControl.updateItemQuality(sulfuras);

		assertEquals(80, sulfuras.quality);
	}

}
