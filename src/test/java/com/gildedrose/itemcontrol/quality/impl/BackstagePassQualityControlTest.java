package com.gildedrose.itemcontrol.quality.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static com.gildedrose.ItemBuilder.initBuilder;
import static com.gildedrose.util.Util.BACKSTAGE_PASSES_ITEM_NAME;

import com.gildedrose.Item;

class BackstagePassQualityControlTest {

	private static final int TWENTY_DAYS = 20;
	private static final int TEN_DAYS = 10;
	private static final int FIVE_DAYS = 5;
	private static final int ZERO_DAYS = 0;
	private static final int FIFTEEN_DAYS = 15;
	
	private BackstagePassQualityControl qualityControl;
	private Item backstagePass;

	@BeforeEach
	public void init() {
		qualityControl = new BackstagePassQualityControl();
		backstagePass = initBuilder()
				.name(BACKSTAGE_PASSES_ITEM_NAME)
				.build();
	}
	
	@Test
	public void shouldIncreaseQualityAsDaysGoBy() {
		backstagePass.sellIn = FIFTEEN_DAYS;
		backstagePass.quality = 49;

		qualityControl.updateItemQuality(backstagePass);

		assertEquals(50, backstagePass.quality);
	}
	@Test public void
	shouldAlsoIncreaseQualityAsDaysGoBy() {
		backstagePass.sellIn= TWENTY_DAYS;
		backstagePass.quality= 19;

		qualityControl.updateItemQuality(backstagePass);
		
		assertEquals(20, backstagePass.quality);
	}
	
	@Test public void
	shouldIncreaseQualityByTwoWhenNeedsToBeSoldInTenDaysOrLess() {
		backstagePass.sellIn= TEN_DAYS;
		backstagePass.quality= 10;
		
		qualityControl.updateItemQuality(backstagePass);
		
		assertEquals(12, backstagePass.quality);
	}
	
	@Test public void
	shouldIncreaseQualityByThreeWhenNeedsToBeSoldInFiveDaysOrLess() {
		backstagePass.sellIn= FIVE_DAYS;
		backstagePass.quality= 10;
		
		qualityControl.updateItemQuality(backstagePass);
		
		assertEquals(13, backstagePass.quality);
	}
	
	@Test public void
	shouldSetQualityToZeroAfterConcert() {
		backstagePass.sellIn= ZERO_DAYS;
		backstagePass.quality= 10;
		
		qualityControl.updateItemQuality(backstagePass);
		
		assertEquals(0, backstagePass.quality);
	}
	
	@Test public void
	shouldNeverIncreaseQualityToMoreThanFifty() {
		backstagePass.sellIn= FIVE_DAYS;
		backstagePass.quality= 50;
		
		qualityControl.updateItemQuality(backstagePass);
		
		assertEquals(50, backstagePass.quality);
	}

}
