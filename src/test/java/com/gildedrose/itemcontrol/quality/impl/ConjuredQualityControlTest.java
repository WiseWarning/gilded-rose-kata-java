package com.gildedrose.itemcontrol.quality.impl;

import static org.junit.jupiter.api.Assertions.*;
import static com.gildedrose.ItemBuilder.initBuilder;
import static com.gildedrose.util.Util.CONJURED_ITEM_NAME;

import org.junit.jupiter.api.Test;

import com.gildedrose.Item;

class ConjuredQualityControlTest {

	private ConjuredQualityControl conjuredQualityControl = new ConjuredQualityControl();

	@Test public void
	shouldDecreaseQualityByTwoForAllConjuredItems() {
		Item conjured = initBuilder()
				.name(CONJURED_ITEM_NAME)
				.quality(20)
				.sellIn(10).build();
		
		conjuredQualityControl.updateItemQuality(conjured);
		
		assertEquals(18, conjured.quality);
	}

	@Test public void
	shouldNeverSetQualityForLessThanZero() {
		Item conjured = initBuilder()
				.name(CONJURED_ITEM_NAME)
				.quality(0)
				.sellIn(10).build();
		
		conjuredQualityControl.updateItemQuality(conjured);
		
		assertEquals(0, conjured.quality);
	}

}
