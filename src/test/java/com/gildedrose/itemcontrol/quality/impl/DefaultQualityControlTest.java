package com.gildedrose.itemcontrol.quality.impl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.gildedrose.Item;

import static com.gildedrose.ItemBuilder.initBuilder;

class DefaultQualityControlTest {

	private DefaultQualityControl defaultQualityControl = new DefaultQualityControl();

	@Test
	public void shouldDecreaseQualityTwice() {
		Item sulfuras = initBuilder().name("+5 Dexterity Vest").quality(20).sellIn(-1).build();

		defaultQualityControl.updateItemQuality(sulfuras);

		assertEquals(18, sulfuras.quality);
	}

	@Test
	public void shouldDecreaseQuality() {
		Item sulfuras = initBuilder().name("Elixir of the Mongoose").quality(7).sellIn(5).build();

		defaultQualityControl.updateItemQuality(sulfuras);

		assertEquals(6, sulfuras.quality);
	}

	@Test
	public void shouldNeverChangeQualityToo() {
		Item sulfuras = initBuilder().name("foo").quality(20).sellIn(10).build();

		defaultQualityControl.updateItemQuality(sulfuras);

		assertEquals(19, sulfuras.quality);
	}

}
