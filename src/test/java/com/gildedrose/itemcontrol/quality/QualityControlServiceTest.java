package com.gildedrose.itemcontrol.quality;

import static com.gildedrose.ItemBuilder.initBuilder;
import static com.gildedrose.util.Util.AGED_BRIE_ITEM_NAME;
import static com.gildedrose.util.Util.BACKSTAGE_PASSES_ITEM_NAME;
import static com.gildedrose.util.Util.CONJURED_ITEM_NAME;
import static com.gildedrose.util.Util.SULFURAS_ITEM_NAME;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gildedrose.Item;

@ExtendWith(MockitoExtension.class)
class QualityControlServiceTest {

	@Mock
	private QualityControlService qualityControlService;
	private Item foo1;
	private Item foo2;
	private Item agedBrie;
	private Item backstagePass;
	private Item conjure;
	private Item sulfuras;

	@BeforeEach
	public void init() {
		foo1 = initBuilder().name("foo1").quality(1).sellIn(1).build();
		foo2 = initBuilder().name("foo2").quality(1).sellIn(1).build();
		agedBrie = initBuilder().name(AGED_BRIE_ITEM_NAME).quality(15).build();
		backstagePass = initBuilder().name(BACKSTAGE_PASSES_ITEM_NAME).quality(49).build();
		conjure = initBuilder().name(CONJURED_ITEM_NAME).quality(49).sellIn(10).build();
		sulfuras = initBuilder().name(SULFURAS_ITEM_NAME).quality(80).sellIn(10).build();
	}
	
	@Test
	public void testSulfateQuality() {
		qualityControlService.updateQuality(sulfuras);
		verify(qualityControlService).updateQuality(sulfuras);
	}
	
	@Test
	public void testAgedBrieQuality() {
		qualityControlService.updateQuality(agedBrie);
		verify(qualityControlService).updateQuality(agedBrie);
	}
	
	@Test
	public void testBackstagePassQuality() {
		qualityControlService.updateQuality(backstagePass);
		verify(qualityControlService).updateQuality(backstagePass);
	}
	
	@Test
	public void testFoo1Quality() {
		qualityControlService.updateQuality(foo1);
		verify(qualityControlService).updateQuality(foo1);
	}
	
	@Test
	public void testSFoo2Quality() {
		qualityControlService.updateQuality(foo2);
		verify(qualityControlService).updateQuality(foo2);
	}
	@Test
	public void testConjureQuality() {
		qualityControlService.updateQuality(conjure);
		verify(qualityControlService).updateQuality(conjure);
	}

}
