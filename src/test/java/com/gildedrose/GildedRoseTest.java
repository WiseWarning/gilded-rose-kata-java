package com.gildedrose;

import static com.gildedrose.ItemBuilder.initBuilder;
import static org.mockito.Mockito.verify;
import static com.gildedrose.util.Util.AGED_BRIE_ITEM_NAME;
import static com.gildedrose.util.Util.BACKSTAGE_PASSES_ITEM_NAME;
import static com.gildedrose.util.Util.CONJURED_ITEM_NAME;
import static com.gildedrose.util.Util.SULFURAS_ITEM_NAME;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gildedrose.itemcontrol.quality.QualityControlService;
import com.gildedrose.itemcontrol.quality.impl.DefaultQualityControl;
import com.gildedrose.itemcontrol.sellin.SellInControl;

@ExtendWith(MockitoExtension.class)
class GildedRoseTest {

	@Mock
	private QualityControlService qualityControlService;
	@Mock
	private DefaultQualityControl qualityControl;
	@Mock
	private SellInControl sellInControl;
	private GildedRose gildedRose;
	private Item foo1;
	private Item foo2;
	private Item agedBrie;
	private Item backstagePass;
	private Item conjure;
	private Item sulfuras;

	@BeforeEach
	public void init() {
		gildedRose = new GildedRose(qualityControlService, sellInControl);

		foo1 = initBuilder().name("foo1").quality(1).sellIn(1).build();
		foo2 = initBuilder().name("foo2").quality(1).sellIn(1).build();
		agedBrie = initBuilder().name(AGED_BRIE_ITEM_NAME).quality(15).build();
		backstagePass = initBuilder().name(BACKSTAGE_PASSES_ITEM_NAME).build();
		conjure = initBuilder().name(CONJURED_ITEM_NAME).quality(20).sellIn(10).build();
		sulfuras = initBuilder().name(SULFURAS_ITEM_NAME).quality(80).sellIn(10).build();
	}

	@Test
	public void shouldUpdateItemsQuality() {
		gildedRose.updateQuality(listOfItem(foo1, foo2, agedBrie, backstagePass, conjure, sulfuras));

		verifyIfQualityWasUpdatedFor(foo1, foo2, agedBrie, backstagePass, conjure, sulfuras);
	}

	@Test
	public void shouldUpdateItemsSellIn() {
		gildedRose.updateQuality(listOfItem(foo1, foo2, agedBrie, backstagePass, conjure, sulfuras));

		verifyIfSellInWasUpdatedFor(foo1, foo2, agedBrie, backstagePass, conjure, sulfuras);
	}

	private void verifyIfSellInWasUpdatedFor(Item... items) {
		for (Item item : items) {
			verify(sellInControl).updateItemSellIn(item);
		}
	}

	private void verifyIfQualityWasUpdatedFor(Item... items) {
		for (Item item : items) {
			verify(qualityControlService).updateQuality(item);
		}
	}

	private List<Item> listOfItem(Item... items) {
		return Arrays.asList(items);
	}

}
